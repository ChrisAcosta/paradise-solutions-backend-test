<?php
    // Data Base connection
    require('dataBaseConnect.php');


    // ARRAYS DEFINITION
    $array_tours = array();
    $array_discounts = array();

    // CALLING TO STORE PROCEDURES
    $call_tours = "CALL ObtainTours;";
    $call_discounts = "CALL ObtainDiscounts;";

    // FILLING THE ARRAY VARIABLE
    foreach($connection->query($call_tours, PDO::FETCH_ASSOC) as $item) $array_tours[] = $item;

    foreach($connection->query($call_discounts, PDO::FETCH_ASSOC) as $item) $array_discounts[] = $item;


    $result = [
        'tours'         => $array_tours,
        'discounts'     => $array_discounts,
    ];


    //Output header
	header('Content-type: application/json');
    echo json_encode($result);


?>

