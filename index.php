<?php

// Get cURL resource
$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, [
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'http://localhost/ps-test/web-service/services.php',
    CURLOPT_USERAGENT => 'Codular Sample cURL Request'
]);
$response = curl_exec($curl);

$tours = json_decode($response, true);

//curl_close($curl);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paradise Solutions - Back End Developer Test</title>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/custom.css">

</head>
<body>

    <div class="paradise container-fluid d-flex justify-content-center align-items-center text-center">
        <div>
            <span><h1>Paradise Solutions</h1></span>
            <span><h3>Back End Developer Test</h3></span>
        </div>
    </div>

    <div class="container text-center">
        <div class="row">
            <?php
                foreach($tours as $table => $item) {
                    echo "<div class='col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mb-5'><div class='table-column h-100 card my-5 pt-3'>";
                    echo "<h1 class='text-uppercase'>$table</h1>";
                    if (is_array($item)) {
                        foreach($item as $key => $value) {
                            if (is_array($value)) {
                                foreach($value as $node => $val) {
                                    echo "<p><b>$node</b>: $val</p>";
                                }
                            } else {
                                echo "<p><b>$key</b>: $value</p>";
                            }
                        }
                    }
                    echo "</div></div>";
                }
            ?>
        </div>
    </div>

</body>
</html>